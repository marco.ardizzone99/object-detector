# Object Detector

*My project for "Social Media Management"'s exam @ University of Catania*

- On "/Hololens2's scripts" there are **C#** scripts for Hololens2.
- On "Server's scripts" there are **python** scripts for receiving data (server.py), interpret data (stringConverter.py) and a Google Colab Notebook (Apply_Detectrn2_on_image.ipynb), used for object detection. 
- On "Data", there is a .rar file, which contains images used in this project and "logs2D.csv", used in Apply_Detectrn2_on_image.ipynb for tracking hand and eye gaze.