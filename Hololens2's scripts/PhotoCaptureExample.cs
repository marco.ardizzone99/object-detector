﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using UnityEngine.Windows.WebCam;
using System.IO;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using System.Globalization;

public class PhotoCaptureExample : MonoBehaviour
{

    public string IP;
    public int port;
    public Text connectButtonText;
    public TextMesh debugText;

    private PhotoCapture photoCaptureObject = null;
    private Texture2D targetTexture = null;
    private Resolution cameraResolution;

    private TCPTestClient client = null;

    string msgToSend;

    //does this in order to prevent comma in floating point numbers, it causes errors in .csv files
    public NumberFormatInfo nfi;


    public static Quaternion ExtractRotation(Matrix4x4 matrix)
    {
        Vector3 forward;
        forward.x = matrix.m02;
        forward.y = matrix.m12;
        forward.z = matrix.m22;

        Vector3 upwards;
        upwards.x = matrix.m01;
        upwards.y = matrix.m11;
        upwards.z = matrix.m21;

        return Quaternion.LookRotation(forward, upwards);
    }

    public static Vector3 ExtractPosition(Matrix4x4 matrix)
    {
        Vector3 position;
        position.x = matrix.m03;
        position.y = matrix.m13;
        position.z = matrix.m23;
        return position;
    }

    public static Vector3 ExtractScale(Matrix4x4 matrix)
    {
        Vector3 scale;
        scale.x = new Vector4(matrix.m00, matrix.m10, matrix.m20, matrix.m30).magnitude;
        scale.y = new Vector4(matrix.m01, matrix.m11, matrix.m21, matrix.m31).magnitude;
        scale.z = new Vector4(matrix.m02, matrix.m12, matrix.m22, matrix.m32).magnitude;
        return scale;
    }


    // Use this for initialization
    public void Start()
    {
        cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();
        //debugText.text = cameraResolution.width.ToString() + " " + cameraResolution.height.ToString();
        //targetTexture = new Texture2D(cameraResolution.width, cameraResolution.height);  //default hololens resolution is 3904x2196, it's too much
        targetTexture = new Texture2D(1280, 720);
        // targetTexture = new Texture2D(480, 270);
        // InputManager.Instance.PushFallbackInputHandler(gameObject);
        nfi = new NumberFormatInfo();
        nfi.NumberDecimalSeparator = ".";
    }
    /*public void Update()
    {
        if (client != null)
        {
            TakeImage();
        }
    }*/

    public void setClient(TCPTestClient _client)
    {
        this.client = _client;

    }

    void OnStoppedPhotoMode(PhotoCapture.PhotoCaptureResult result)
    {
        photoCaptureObject.Dispose();
        photoCaptureObject = null;
    }


    public void GetHandData(Handedness yourHand, out MixedRealityPose pose, Transform cameraRGBTrasform)
    {
        Camera cam = Camera.main;

        if (HandJointUtils.TryGetJointPose(TrackedHandJoint.ThumbTip, yourHand, out pose))
        {
            Vector3 PalmPosLocalMainCamera = cam.transform.InverseTransformPoint(pose.Position);
            msgToSend += (PalmPosLocalMainCamera.x).ToString(nfi) + " ";     //adding nfi in ToString() in order to write point separated number instead of comma separated ones
            msgToSend += (PalmPosLocalMainCamera.y).ToString(nfi) + " ";
            msgToSend += (PalmPosLocalMainCamera.z).ToString(nfi) + " ";
        }
        else
        {
            msgToSend += "-1 -1 -1 ";
        }

        if (HandJointUtils.TryGetJointPose(TrackedHandJoint.IndexTip, yourHand, out pose))
        {
            Vector3 PalmPosLocalMainCamera = cam.transform.InverseTransformPoint(pose.Position);
            msgToSend += (PalmPosLocalMainCamera.x).ToString(nfi) + " ";     //adding nfi in ToString() in order to write point separated number instead of comma separated ones
            msgToSend += (PalmPosLocalMainCamera.y).ToString(nfi) + " ";
            msgToSend += (PalmPosLocalMainCamera.z).ToString(nfi) + " ";
        }
        else
        {
            msgToSend += "-1 -1 -1 ";
        }

        if (HandJointUtils.TryGetJointPose(TrackedHandJoint.MiddleTip, yourHand, out pose))
        {
            Vector3 PalmPosLocalMainCamera = cam.transform.InverseTransformPoint(pose.Position);
            msgToSend += (PalmPosLocalMainCamera.x).ToString(nfi) + " ";     //adding nfi in ToString() in order to write point separated number instead of comma separated ones
            msgToSend += (PalmPosLocalMainCamera.y).ToString(nfi) + " ";
            msgToSend += (PalmPosLocalMainCamera.z).ToString(nfi) + " ";
        }
        else
        {
            msgToSend += "-1 -1 -1 ";
        }

        if (HandJointUtils.TryGetJointPose(TrackedHandJoint.RingTip, yourHand, out pose))
        {
            Vector3 PalmPosLocalMainCamera = cam.transform.InverseTransformPoint(pose.Position);
            msgToSend += (PalmPosLocalMainCamera.x).ToString(nfi) + " ";     //adding nfi in ToString() in order to write point separated number instead of comma separated ones
            msgToSend += (PalmPosLocalMainCamera.y).ToString(nfi) + " ";
            msgToSend += (PalmPosLocalMainCamera.z).ToString(nfi) + " ";
        }
        else
        {
            msgToSend += "-1 -1 -1 ";
        }

        if (HandJointUtils.TryGetJointPose(TrackedHandJoint.PinkyTip, yourHand, out pose))
        {
            Vector3 PalmPosLocalMainCamera = cam.transform.InverseTransformPoint(pose.Position);
            msgToSend += (PalmPosLocalMainCamera.x).ToString(nfi) + " ";     //adding nfi in ToString() in order to write point separated number instead of comma separated ones
            msgToSend += (PalmPosLocalMainCamera.y).ToString(nfi) + " ";
            msgToSend += (PalmPosLocalMainCamera.z).ToString(nfi) + " ";
        }
        else
        {
            msgToSend += "-1 -1 -1 ";
        }
    }

    public void GetEyeGaze()
    {
        Camera cam = Camera.main;

        Vector3 gazePos = CoreServices.InputSystem.EyeGazeProvider.GazeOrigin + CoreServices.InputSystem.EyeGazeProvider.GazeDirection.normalized;
        Vector3 gazePosCamera = cam.transform.InverseTransformPoint(gazePos);
        msgToSend += (gazePosCamera.x).ToString(nfi) + " ";
        msgToSend += (gazePosCamera.y).ToString(nfi) + " ";
        msgToSend += (gazePosCamera.z).ToString(nfi) + " ";
    }

    void OnCapturedPhotoToMemory(PhotoCapture.PhotoCaptureResult result, PhotoCaptureFrame photoCaptureFrame)
    {
        Matrix4x4 projectionMatrix;
        photoCaptureFrame.TryGetProjectionMatrix(out projectionMatrix);

        Matrix4x4 cameraToWorldMatrix;
        photoCaptureFrame.TryGetCameraToWorldMatrix(out cameraToWorldMatrix);

        Matrix4x4 worldToCameraMatrix = cameraToWorldMatrix.inverse;

        Transform cameraRGBTrasform = this.transform;
        cameraRGBTrasform.position = ExtractPosition(worldToCameraMatrix);
        cameraRGBTrasform.rotation = ExtractRotation(worldToCameraMatrix);
        cameraRGBTrasform.localScale = ExtractScale(worldToCameraMatrix);

        long ldap = DateTimeOffset.Now.ToUnixTimeMilliseconds() + 60 * 60 * 2;
        string str = "0.###########";     //does this in order to prevent scientific notation
        str = ldap.ToString();
        msgToSend += str + " ";


        MixedRealityPose pose;

        GetEyeGaze();
        GetHandData(Handedness.Left, out pose, cameraRGBTrasform);
        GetHandData(Handedness.Right, out pose, cameraRGBTrasform);
        client.SendMsg(msgToSend);
        msgToSend = "";
        //HandJointUtils.TryGetJointPose(TrackedHandJoint.Palm, Handedness.Right, out posePalm);

        //Vector3 PalmPosLocal = cameraRGBTrasform.InverseTransformPoint(pose.Position);


        //Camera cam = Camera.main;

        //Vector3 PalmPosScreen = cam.WorldToScreenPoint(posePalm.Position);
        //Vector3 PalmPosLocalMainCamera = cam.transform.InverseTransformPoint(posePalm.Position);

        //Debug.Log("Coordinate locali palm: " + PalmPosLocal.ToString());
        //Debug.Log("PalmPosLocalMainCamera " + PalmPosLocalMainCamera.ToString());

        //Debug.Log("Coordinate screen palm: " + PalmPosScreen.ToString());   //non funziona come dovrebbe

        photoCaptureFrame.UploadImageDataToTexture(targetTexture);
        //byte[] texByte = targetTexture.EncodeToJPG();
        //byte[] image = new byte[texByte.Length];
        //Array.Copy(texByte, image, texByte.Length);
        Debug.Log("Invio l'immagine al server.");

        //byte[] bytes = targetTexture.EncodeToPNG();
        byte[] bytes = targetTexture.EncodeToJPG();
        //var dirPath = "C:/Users/marco/Desktop/";
        //File.WriteAllBytes(dirPath + "Image" + ".png", bytes);

        //byte[] image = targetTexture.GetRawTextureData();

        client.SendImage(bytes);


       
        photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
    }

    public void TakeImage()
    {
        if (this.client != null)
        {
            PhotoCapture.CreateAsync(false, delegate (PhotoCapture captureObject)   //true -> Holograms, false -> No Holograms
            {
                Debug.Log("Entered in TakeImage()");
                photoCaptureObject = captureObject;
                CameraParameters cameraParameters = new CameraParameters();
                cameraParameters.hologramOpacity = 0.9f;
                cameraParameters.cameraResolutionWidth = cameraResolution.width;
                cameraParameters.cameraResolutionHeight = cameraResolution.height;
                cameraParameters.pixelFormat = CapturePixelFormat.BGRA32;
                photoCaptureObject.StartPhotoModeAsync(cameraParameters, delegate (PhotoCapture.PhotoCaptureResult result)
                {
                    photoCaptureObject.TakePhotoAsync(OnCapturedPhotoToMemory);
                });
            });
        }
    }
}