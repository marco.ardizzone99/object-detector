import csv
import numpy as np
import math
import os

focal_l = 1300
center_x = 1280/2
center_y = 720/2

def coord_converter(str):
    j=0
    converted_str = []
    for i in range(0,11):                  #3 coordinate per occhi + 3*5*2 coordinate per  dita
        point=[float(str[i+j]), float(str[i+j+1]), float(str[i+j+2])]
        if(point[0]==-1 or point[1]==-1 or point[2]==-1):
            j+=2
            continue                       #se un punto è uguale a -1, allora lo skippo

        x = focal_l * ((1.52048 * point[0]) / point[2]) 
        y = focal_l * ((1.52048 * point[1]) / point[2])
        x += 1280 / 2
        y = (720 / 2) - y

        x2 = (int)(x-np.sign(x-center_x)*math.sqrt(abs(x-center_x)*16))
        y2 = (int)(y-np.sign(y-center_y)*math.sqrt(abs(y-center_y)*9))
        converted_str.append(x2)
        converted_str.append(y2)
        j+=2
    return converted_str

with open('C:/Users/marco/Desktop/Tesi/sock_receive/logs.csv', 'r', encoding='UTF8', newline='') as myfile, open('C:/Users/marco/Desktop/Tesi/sock_receive/logs2D.csv', 'a', encoding='UTF8', newline='') as mynewfile:
    reader = csv.reader(myfile, escapechar=' ')
    wr = csv.writer(mynewfile, quoting=csv.QUOTE_NONE, escapechar=' ')
    for row in reader:
            n_row = []
            n_row = coord_converter(row[1:])
            #n_row = n_row[:-1]
            n_row.insert(0, row[0])
            if(len(n_row) == 3):
                os.remove('C:/Users/marco/Desktop/Tesi/sock_receive/' + n_row[0] + '.jpg')

                #print("i dont append this cause its only eye gaze")
                continue
            #print(n_row)
            wr.writerow(n_row)


#                if(all(element == -1 for element in received_data[4:-1])):        #if everything but timestamp is equal to -1, don't save it
#                    continue